Application contains bottom navigation menu with two options. 

First options is for your saved recipes. These recipes are stored in device. You can see list of saved recipes and after click also detail of recipe. Here you can remove recipe from your favourite recipes. 

Second option is for searching recipes from rapidapi.com api. You can choose combination of searched tags (api provide response with recipes that contain all of selected tags).
After search you can see similar functiona UI like with locally saved recipes. Here you can save recipe to your favourite recipes. 


