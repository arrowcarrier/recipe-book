package arrowcarrier.recepty

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RecipeBookApplication : Application()