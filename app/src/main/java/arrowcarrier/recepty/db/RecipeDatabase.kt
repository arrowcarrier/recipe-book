package arrowcarrier.recepty.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import arrowcarrier.recepty.dataClasses.*

@Database(entities =
[DatabaseRecipe::class,
    DatabaseTag::class,
    DatabaseInstruction::class,
    DatabaseIngredient::class,
    DatabaseNutrition::class,
    RecipeTagRelation::class],
    version = 1)
@TypeConverters(BitmapTypeConvertor::class)
abstract class RecipeDatabase : RoomDatabase() {
    abstract fun recipeDao() : RecipeDAO
}