package arrowcarrier.recepty.db

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.room.TypeConverter
import java.io.ByteArrayOutputStream

class BitmapTypeConvertor {

    @TypeConverter
    fun fromBitmap(bitmap : Bitmap) : ByteArray {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
        return outputStream.toByteArray()
    }

    @TypeConverter
    fun toBitmap(array: ByteArray) : Bitmap {
        return BitmapFactory.decodeByteArray(array, 0, array.size)
    }
}