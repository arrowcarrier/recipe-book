package arrowcarrier.recepty.db

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import arrowcarrier.recepty.dataClasses.*

data class RecipeDBDetailRelation(
    @Embedded val recipe: DatabaseRecipe,
    @Relation(
        parentColumn = "recipeId",
        entityColumn = "tagName",
        associateBy = Junction(RecipeTagRelation::class)
    )
    val tags : List<DatabaseTag>,
    @Relation(
        parentColumn = "recipeId",
        entityColumn = "recipe"
    )
    val ingredients : List<DatabaseIngredient>,
    @Relation(
        parentColumn = "recipeId",
        entityColumn = "recipe"
    )
    val instructions : List<DatabaseInstruction>,
    @Relation(
        parentColumn = "recipeId",
        entityColumn = "recipe"
    )
    val nutritions : DatabaseNutrition?
)