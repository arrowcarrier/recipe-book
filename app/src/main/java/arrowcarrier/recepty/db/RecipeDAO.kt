package arrowcarrier.recepty.db

import androidx.paging.PagingSource
import androidx.room.*
import arrowcarrier.recepty.dataClasses.*

@Dao
interface RecipeDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertRecipe(recipe: DatabaseRecipe) : Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertTag(tag: DatabaseTag)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertIngredients(ingredients: List<DatabaseIngredient>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInstructions(instructions: List<DatabaseInstruction>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNutrition(nutrition: DatabaseNutrition)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRecipeTagRelation(recipeTagRelation: RecipeTagRelation)

    @Transaction
    @Query ("SELECT EXISTS(SELECT * FROM recipes WHERE recipeName = :recipeName)")
    suspend fun containRecipe(recipeName : String) : Boolean

    @Transaction
    @Query("SELECT * FROM recipes")
    fun selectMyRecipes() : PagingSource<Int, DatabaseRecipe>

    @Transaction
    @Query("SELECT * FROM recipes WHERE recipeId = :recipeId")
    suspend fun selectRecipeDetailRelation(recipeId : Long) : RecipeDBDetailRelation

    @Delete
    suspend fun deleteRecipeWithIngredientAndInstructions(recipe : DatabaseRecipe,
                                                          ingredients: List<DatabaseIngredient>,
                                                          instructions: List<DatabaseInstruction>)

    @Delete
    suspend fun deleteNutrition(nutrition : DatabaseNutrition)

    @Transaction
    @Query("DELETE FROM recipe_tag WHERE recipeId = :recipeId")
    suspend fun deleteRecipeTagRelations(recipeId : Long)
    
}