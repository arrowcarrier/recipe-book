package arrowcarrier.recepty.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainMotionStateViewModel : ViewModel() {

    lateinit var enabledTags : List<String>

    private val _isAnimationStart : MutableLiveData<Boolean> = MutableLiveData(true)
        val isAnimationStart : LiveData<Boolean> = _isAnimationStart

    fun changeAnimationState() {
        val currentState = _isAnimationStart.value
        currentState?.let { _isAnimationStart.postValue(!it) }
    }
}