package arrowcarrier.recepty.viewmodels

import arrowcarrier.recepty.dataClasses.RecipeDetail

object ViewModelService {

    fun getRecipeGroupViewModel(recipeGroup : RecipeDetail) : RecipeGroupViewModelFactory {
        return RecipeGroupViewModelFactory(recipeGroup)
    }
}