package arrowcarrier.recepty.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import arrowcarrier.recepty.dataClasses.RecipeDetail

class RecipeGroupViewModel(recipe : RecipeDetail) : ViewModel() {

    private val _actualRecipe : MutableLiveData<RecipeDetail> = MutableLiveData()
    val actualRecipe : LiveData<RecipeDetail> = _actualRecipe

    init {
        _actualRecipe.postValue(recipe)
    }
}