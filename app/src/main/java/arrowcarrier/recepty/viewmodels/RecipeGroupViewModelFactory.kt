package arrowcarrier.recepty.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import arrowcarrier.recepty.dataClasses.RecipeDetail

class RecipeGroupViewModelFactory(private val recipeGroup : RecipeDetail) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RecipeGroupViewModel(recipeGroup) as T
    }
}

