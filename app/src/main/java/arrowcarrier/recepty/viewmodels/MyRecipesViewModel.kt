package arrowcarrier.recepty.viewmodels

import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import arrowcarrier.recepty.dataClasses.DatabaseRecipe
import arrowcarrier.recepty.db.RecipeDAO
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class MyRecipesViewModel @Inject constructor(
    private val recipeDao : RecipeDAO
) : ViewModel() {

    fun searchDbPaging() : Flow<PagingData<DatabaseRecipe>> {
        return Pager(
            config = PagingConfig(
                pageSize = 50,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {recipeDao.selectMyRecipes()}
        ).flow
    }

}