package arrowcarrier.recepty.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import arrowcarrier.recepty.dataClasses.DatabaseInstruction
import arrowcarrier.recepty.dataClasses.DatabaseTag
import arrowcarrier.recepty.dataClasses.RecipeDetail
import arrowcarrier.recepty.dataClasses.RecipeTagRelation
import arrowcarrier.recepty.db.RecipeDAO
import arrowcarrier.recepty.util.*
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class RecipeNetworkViewModel @Inject constructor(
    dataSourceFactory: DefaultDataSourceFactory,
    player : SimpleExoPlayer,
    recipeDao : RecipeDAO
) : RecipeDetailViewModel(dataSourceFactory, player, recipeDao) {

    private val _actualRecipe : MutableLiveData<RecipeDetail> = MutableLiveData()
    val actualRecipe : LiveData<RecipeDetail> = _actualRecipe

    fun setRecipeDetail(recipe : RecipeDetail) {
        if (_actualRecipe.value == null) {
            _actualRecipe.postValue(recipe)
            if (recipe.video != null && recipe.video != "") {
                val source = ProgressiveMediaSource.Factory(factory)
                    .createMediaSource(MediaItem.fromUri(recipe.video))

                player.setMediaSource(source)
                player.prepare()
            }
        }
    }

    fun saveRecipeDetail(enabledTags: List<String>) {
        _recipeOperationState.postValue(LivedataResponse.Loading)
        val actualRecipe = _actualRecipe.value
        actualRecipe?.let { recipe ->
            viewModelScope.launch {
                try {
                    val recipeId = saveRecipe(recipe)
                    if (recipeId != -1L) {
                        saveRecipeIngredients(recipe, recipeId)
                        saveRecipeInstructions(recipe, recipeId)
                        saveRecipeNutrition(recipe, recipeId)
                        saveRecipeTagsWithRelations(recipe, recipeId, enabledTags)
                        _recipeOperationState.postValue(LivedataResponse.Success(Event(recipe.name)))
                    } else {
                        _recipeOperationState.postValue(LivedataResponse.Error(Event(recipe.name), null))
                    }
                } catch (e: Exception) {
                    _recipeOperationState.postValue(LivedataResponse.Error(Event(recipe.name), e))
                }
            }
        }
    }

    private suspend fun saveRecipe(detail : RecipeDetail) : Long {
        return recipeDao.insertRecipe(detail.asDatabaseRecipe())
    }

    private suspend fun saveRecipeIngredients(recipe : RecipeDetail, recipeId : Long) {
        recipeDao.insertIngredients(recipe.ingredients.asDatabaseIngredientList(recipeId))
    }

    private suspend fun saveRecipeInstructions(recipe : RecipeDetail, recipeId : Long) {
        val instructions = recipe.instructions.map { DatabaseInstruction(0, recipeId, it.text) }
        recipeDao.insertInstructions(instructions)
    }

    private suspend fun saveRecipeNutrition(recipe : RecipeDetail, recipeId : Long) {
        recipe.nutrition?.let {  nutrition ->
            if (nutrition.isValid()) recipeDao.insertNutrition(nutrition.asDatabaseNutrition(recipeId))
        }
    }

    private suspend fun saveRecipeTagsWithRelations(recipe : RecipeDetail, recipeId : Long, enabledTags: List<String>) {
        recipe.tags.forEach { newTag ->
            if (enabledTags.contains(newTag.name)) {
                recipeDao.insertTag(DatabaseTag(newTag.name, newTag.type, newTag.displayName))
                val tagRelation = RecipeTagRelation(recipeId, newTag.name)
                recipeDao.insertRecipeTagRelation(tagRelation)
            }
        }
    }

    suspend fun containsRecipe(recipeName : String) : Boolean {
        return recipeDao.containRecipe(recipeName)
    }
}