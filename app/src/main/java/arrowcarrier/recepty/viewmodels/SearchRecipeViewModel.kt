package arrowcarrier.recepty.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import arrowcarrier.recepty.dataClasses.RecipeDetail
import arrowcarrier.recepty.dataClasses.Tag
import arrowcarrier.recepty.network.RetrofitService
import arrowcarrier.recepty.network.SearchRecipePagingSource
import arrowcarrier.recepty.util.LivedataResponse
import arrowcarrier.recepty.util.getCheckedTagsAsString
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class SearchRecipeViewModel @Inject constructor(
    private val retrofitService : RetrofitService,
    private val availableTagArray : Array<String>
) : ViewModel() {

    private val _tagResponse : MutableLiveData<LivedataResponse<List<Tag>>> = MutableLiveData()
    val tagResponse : LiveData<LivedataResponse<List<Tag>>> = _tagResponse

    private val tagGroupsList : Array<MutableList<Tag>> = Array(availableTagArray.size) { ArrayList() }

    private var currentSearchFlow : Flow<PagingData<RecipeDetail>>? = null

    val checkedTags = ArrayList<Tag>()

    private var currentTags : String = ""

    var motionProgress = 0f

    init {
        loadTags()
    }

    fun searchAPIPaging() : Flow<PagingData<RecipeDetail>> {
        val newTags = checkedTags.getCheckedTagsAsString(false)
        val lastSearch = currentSearchFlow
        if (newTags == currentTags && lastSearch != null) {
            return lastSearch
        }
        currentTags = newTags
        val newSearch : Flow<PagingData<RecipeDetail>> = Pager(
            config = PagingConfig(
                pageSize = 40,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { SearchRecipePagingSource(retrofitService, newTags) }
        ).flow.cachedIn(viewModelScope)
        currentSearchFlow = newSearch
        return newSearch
    }

    fun isReloadNeeded() : Boolean {
        return currentSearchFlow != null
    }

    fun loadTags() {
        viewModelScope.launch {
            _tagResponse.postValue(LivedataResponse.Loading)
            try {
                val result = retrofitService.loadTags()
                if (result.isSuccessful) {
                    result.body()?.tags?.forEach { tag ->
                        val index = availableTagArray.indexOf(tag.type)
                        if (index != -1) tagGroupsList[index].add(tag)
                        _tagResponse.postValue(LivedataResponse.Success<List<Tag>>(tagGroupsList[0]))
                    }
                } else {
                    _tagResponse.postValue(LivedataResponse.Error(null, IllegalStateException(result.message())))
                }
            } catch (e : IOException) {
                _tagResponse.postValue(LivedataResponse.Error(null, e))
            }
        }
    }

    fun getTagGroup(index : Int) : List<Tag>? {
        return if (index != -1) tagGroupsList[index] else null
    }

    fun changeMotionProgress() {
        motionProgress = if (motionProgress == 1f) 0f else 1f
    }

    fun getEnabledTags() : List<String> {
        val list = ArrayList<String>()
        tagGroupsList.forEach { groupList ->
            groupList.forEach { tag ->
                list.add(tag.name)
            }
        }
        return list
    }
}