package arrowcarrier.recepty.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import arrowcarrier.recepty.db.RecipeDAO
import arrowcarrier.recepty.db.RecipeDBDetailRelation
import arrowcarrier.recepty.util.Event
import arrowcarrier.recepty.util.LivedataResponse
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class RecipeDbViewModel @Inject constructor(
    dataSourceFactory: DefaultDataSourceFactory,
    player : SimpleExoPlayer,
    recipeDao : RecipeDAO
) : RecipeDetailViewModel(dataSourceFactory, player, recipeDao) {

    private val _actualRecipe : MutableLiveData<RecipeDBDetailRelation> = MutableLiveData()
    val actualRecipe : LiveData<RecipeDBDetailRelation> = _actualRecipe

    fun findRecipeDetail(recipeId : Long) {
        viewModelScope.launch {
            val job = async {
                recipeDao.selectRecipeDetailRelation(recipeId)
            }
            val relation = job.await()
            _actualRecipe.postValue(relation)
            if (relation.recipe.video != null && relation.recipe.video != "") {
                val source = ProgressiveMediaSource.Factory(factory)
                    .createMediaSource(MediaItem.fromUri(relation.recipe.video))

                player.setMediaSource(source)
                player.prepare()
            }
        }
    }

    fun deleteRecipeFromDatabase() {
        val actualRelation = _actualRecipe.value
        actualRelation?.let {  relation ->
            _recipeOperationState.postValue(LivedataResponse.Loading)
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    recipeDao.deleteRecipeWithIngredientAndInstructions(
                        relation.recipe,
                        relation.ingredients,
                        relation.instructions
                    )
                    relation.nutritions?.let { recipeDao.deleteNutrition(it) }
                    recipeDao.deleteRecipeTagRelations(relation.recipe.recipeId)
                    _recipeOperationState.postValue(LivedataResponse.Success(Event(relation.recipe.recipeName)))
                } catch (e: Exception) {
                    _recipeOperationState.postValue(
                        LivedataResponse.Error(
                            Event(relation.recipe.recipeName),
                            e
                        )
                    )
                }
            }
        }
    }

}