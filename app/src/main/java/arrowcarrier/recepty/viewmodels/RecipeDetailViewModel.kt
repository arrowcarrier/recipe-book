package arrowcarrier.recepty.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import arrowcarrier.recepty.db.RecipeDAO
import arrowcarrier.recepty.util.Event
import arrowcarrier.recepty.util.LivedataResponse
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory

abstract class RecipeDetailViewModel(protected val factory : DefaultDataSourceFactory,
    val player : SimpleExoPlayer,
    protected val recipeDao : RecipeDAO) : ViewModel() {

    var lastOpenedMenuFragment : Int? = null

    var isMenuRolledUp : Boolean = false
        private set

    private var isVideoPlaying : Boolean = true

    private val _isVideoFullscreen : MutableLiveData<Boolean> = MutableLiveData(false)
        val isVideoFullscreen : LiveData<Boolean> = _isVideoFullscreen

    protected val _recipeOperationState : MutableLiveData<LivedataResponse<Event<String>>> = MutableLiveData()
    val recipeOperationState : LiveData<LivedataResponse<Event<String>>> = _recipeOperationState

    fun stopVideo() {
        isVideoPlaying = player.playWhenReady
        player.playWhenReady = false
    }

    fun resumeVideo() {
        player.playWhenReady = isVideoPlaying
    }

    fun changeVideoFullscreen() {
        val actualState = _isVideoFullscreen.value
        actualState?.let { if (it) _isVideoFullscreen.postValue(false) else _isVideoFullscreen.postValue(true) }
    }

    fun changeMenuState() {
        isMenuRolledUp = !isMenuRolledUp
    }

    override fun onCleared() {
        super.onCleared()
        player.release()
    }
}