package arrowcarrier.recepty.dataClasses

import android.graphics.Bitmap
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "recipes", indices = [Index(value = ["recipeName"], unique = true)])
data class DatabaseRecipe (
    @PrimaryKey(autoGenerate = true) val recipeId : Long,
    val recipeName : String,
    val description : String? = null,
    val thumbnail : Bitmap? = null,
    val video : String? = "",
    val portions : Int,
)

@Entity(tableName = "instructions")
data class DatabaseInstruction (
    @PrimaryKey(autoGenerate = true) val instructionId : Long,
    val recipe : Long,
    val text : String,
)

@Entity(tableName = "ingredients")
data class DatabaseIngredient(
    @PrimaryKey(autoGenerate = true) val ingredientId : Long,
    val recipe : Long,
    val text : String
)

@Entity(tableName = "nutritions")
data class DatabaseNutrition(
    @PrimaryKey(autoGenerate = false) val recipe : Long,
    val calories : Int,
    val carbohydrates : Int,
    val fat : Int,
    val protein : Int,
    val sugar : Int,
    val fiber : Int
)

@Entity(tableName = "tags")
data class DatabaseTag @JvmOverloads constructor(
    @PrimaryKey(autoGenerate = false)  val tagName : String,
    val type : String,
    val displayName : String = "",
)

@Entity(tableName = "recipe_tag", primaryKeys = ["recipeId", "tagName"])
data class RecipeTagRelation(
    val recipeId : Long,
    val tagName : String
)