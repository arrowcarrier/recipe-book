package arrowcarrier.recepty.dataClasses

import android.graphics.Bitmap
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class TagList(
    @SerializedName("results") val tags : List<Tag>
)

@Parcelize
data class Tag(
    @SerializedName("display_name") val displayName : String,
    @SerializedName("type") val type : String,
    @SerializedName("name") val name : String = "",
    var isChecked : Boolean = false
) : Parcelable

data class RecipeList(
    @SerializedName("count") val resultsCount : Int,
    @SerializedName("results") val recipes : List<RecipeDetail>
)

@Parcelize
data class RecipeDetail(
    @SerializedName("id") val id : Int = 0,
    @SerializedName("name") val name : String = "",
    @SerializedName("thumbnail_url") val thumbnail : String = "",
    @SerializedName("original_video_url") val video : String? = "",
    @SerializedName("instructions") val instructions : List<Instruction>,
    @SerializedName("tags") val tags : List<Tag>,
    @SerializedName("sections") val ingredients : List<Section>,
    @SerializedName("recipes") val recipes : List<RecipeDetail>? = null,
    @SerializedName("num_servings") val portions : Int,
    @SerializedName("nutrition") val nutrition : Nutrition? = null,
    @SerializedName("user_ratings") val ratings : Rating? = null,
    @SerializedName("description") val description : String? = null,
    var recipeImage : Bitmap? = null
) : Parcelable

@Parcelize
data class Instruction(
    @SerializedName("position") val position : Int = 0,
    @SerializedName("display_text") val text : String = ""
) : Parcelable

@Parcelize
data class Section(
    @SerializedName("components") val components : List<Component>
) : Parcelable

@Parcelize
data class Component(
    @SerializedName("raw_text") val text : String = ""
) : Parcelable

@Parcelize
data class Nutrition(
    @SerializedName("calories") val calories : Int,
    @SerializedName("carbohydrates") val carbohydrates : Int,
    @SerializedName("fat") val fat : Int,
    @SerializedName("protein") val protein : Int,
    @SerializedName("sugar") val sugar : Int,
    @SerializedName("fiber") val fiber : Int,
) : Parcelable

@Parcelize
data class Rating(
    @SerializedName("count_positive") val positive : Int,
    @SerializedName("count_negative") val negative : Int
) : Parcelable


