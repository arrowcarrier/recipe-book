package arrowcarrier.recepty.network

import arrowcarrier.recepty.dataClasses.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface RetrofitService {

    @GET("tags/list")
    suspend fun loadTags(
        @Header("x-rapidapi-host") host : String = RAPIDAPI_HOST,
        @Header("x-rapidapi-key") key : String = RAPIDAPI_KEY
    ) : Response<TagList>

    @GET("recipes/list")
    suspend fun loadRecipes(
        @Header("x-rapidapi-host") host : String = RAPIDAPI_HOST,
        @Header("x-rapidapi-key") key : String = RAPIDAPI_KEY,
        @Query("from") from : Int,
        @Query("size") size : Int,
        @Query("tags") tags : String) : Response<RecipeList>

    companion object {

        private const val RAPIDAPI_BASE_URL = "https://tasty.p.rapidapi.com/"
        private const val RAPIDAPI_HOST = "tasty.p.rapidapi.com"
        private const val RAPIDAPI_KEY = "e5b326438emshd12724af90e01a5p1d6dddjsn53236ffa7a2c"


        fun create(): RetrofitService {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()

            return Retrofit.Builder()
                .baseUrl(RAPIDAPI_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build()
                .create(RetrofitService::class.java)
        }
    }
}