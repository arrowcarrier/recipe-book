package arrowcarrier.recepty.network

import androidx.paging.PagingSource
import androidx.paging.PagingState
import arrowcarrier.recepty.dataClasses.*

class SearchRecipePagingSource (private val retrofit : RetrofitService, private val tags : String) : PagingSource<Int, RecipeDetail>() {

    private val PAGE_SIZE = 40

    override fun getRefreshKey(state: PagingState<Int, RecipeDetail>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.prevKey?.plus(PAGE_SIZE)
                ?: state.closestPageToPosition(it)?.nextKey?.minus(PAGE_SIZE)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, RecipeDetail> {
        return try {
            val pageIndex = params.key ?: 0
            val response = retrofit.loadRecipes(from = pageIndex, size = PAGE_SIZE, tags = tags)
            val prevPage : Int?
            val nextPage : Int?
            if (response.isSuccessful) {
                prevPage = if (pageIndex == 0) null else pageIndex
                nextPage = response.body()?.let {
                    if (it.resultsCount < pageIndex + PAGE_SIZE) null else pageIndex + PAGE_SIZE
                }
                val data = response.body()?.recipes!!
                LoadResult.Page(
                    data = data,
                    prevKey = prevPage,
                    nextKey = nextPage
                )
            } else {
                LoadResult.Error(IllegalStateException(response.message()))
            }
        } catch (e : Exception) {
            LoadResult.Error(e)
        }
    }
}