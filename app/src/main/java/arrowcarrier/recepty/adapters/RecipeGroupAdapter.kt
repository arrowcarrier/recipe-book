package arrowcarrier.recepty.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import arrowcarrier.recepty.dataClasses.RecipeDetail
import arrowcarrier.recepty.databinding.ListItemRecipeGroupBinding
import com.bumptech.glide.RequestManager
import javax.inject.Inject

class RecipeGroupAdapter @Inject constructor(private val glide : RequestManager) : RecyclerView.Adapter<RecipeGroupViewHolder>() {

    private lateinit var list : List<RecipeDetail>

    private lateinit var listener : (RecipeDetail) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeGroupViewHolder {
        return RecipeGroupViewHolder(ListItemRecipeGroupBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecipeGroupViewHolder, position: Int) {
        holder.binding.apply {
            root.setOnClickListener {
                listener.invoke(list[position])
            }
            glide.load(list[position].thumbnail)
                .into(ivThumbnailGroup)
            tvNameGroup.text = list[position].name
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setFeed(newList : List<RecipeDetail>) {
        list = newList
        notifyDataSetChanged()
    }

    fun setListener(newListener : (RecipeDetail) -> Unit) {
        listener = newListener
    }
}

class RecipeGroupViewHolder(val binding : ListItemRecipeGroupBinding) : RecyclerView.ViewHolder(binding.root)