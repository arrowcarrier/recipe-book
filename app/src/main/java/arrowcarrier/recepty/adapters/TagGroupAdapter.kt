package arrowcarrier.recepty.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import arrowcarrier.recepty.dataClasses.Tag
import arrowcarrier.recepty.databinding.ListItemTagBinding

class TagGroupAdapter(private var list : List<Tag>,private val itemCheckedCallback : (Tag, Boolean) -> Unit) : RecyclerView.Adapter<TagGroupViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagGroupViewHolder {
        return TagGroupViewHolder(ListItemTagBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: TagGroupViewHolder, position: Int) {
        holder.binding.checkBox.apply {
            text = list[position].displayName
            isChecked = list[position].isChecked
            setOnClickListener {
                list[position].isChecked = isChecked
                itemCheckedCallback(list[position], isChecked)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun changeData(nexList : List<Tag>?) {
        if (nexList != null) {
            list = nexList
            notifyDataSetChanged()
        }
    }
}

class TagGroupViewHolder(val binding : ListItemTagBinding) : RecyclerView.ViewHolder(binding.root)