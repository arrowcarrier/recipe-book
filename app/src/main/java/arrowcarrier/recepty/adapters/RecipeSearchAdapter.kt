package arrowcarrier.recepty.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import arrowcarrier.recepty.R
import arrowcarrier.recepty.dataClasses.RecipeDetail
import arrowcarrier.recepty.databinding.ListItemRecipeSearchBinding
import arrowcarrier.recepty.util.getTagsAsString
import com.bumptech.glide.RequestManager
import javax.inject.Inject


class RecipeSearchAdapter @Inject constructor(private val glide : RequestManager) : PagingDataAdapter<RecipeDetail, RecipeSearchViewHolder>(DIFF_CALLBACK) {

    private lateinit var listener : (RecipeDetail) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeSearchViewHolder {
        return RecipeSearchViewHolder(ListItemRecipeSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecipeSearchViewHolder, position: Int) {
        holder.binding.apply {
            val item = getItem(position)
            item?.let { recipe ->

                root.setOnClickListener {
                    listener.invoke(recipe)
                }
                tvNameSearch.text = recipe.name
                val tagString = holder.binding.root.context.resources.getString(R.string.recipe_tags, recipe.getTagsAsString())
                tvTags.text = tagString
                glide.load(recipe.thumbnail)
                    .into(ivThumbnailSearch)

                recipe.recipes?.let {
                    val str = holder.binding.root.context.resources.getString(R.string.contain_recipes, it.size)
                    tvRecipesNumber.apply {
                        visibility = View.VISIBLE
                        text = str
                    }
                }
                if (recipe.ratings == null || recipe.ratings.positive == 0 && recipe.ratings.negative == 0) {
                    tvNumberOfRatings.text =
                        holder.binding.root.context.resources.getQuantityString(R.plurals.number_of_ratings, 0, 0)
                } else {
                    tvNumberOfRatings.text = holder.binding.root.context.resources.getQuantityString(R.plurals.number_of_ratings,
                        recipe.ratings.positive + recipe.ratings.negative,
                        recipe.ratings.positive + recipe.ratings.negative)
                    val pos = recipe.ratings.positive.toFloat()
                    val neg = recipe.ratings.negative.toFloat()
                    rbRating.apply {
                        visibility = View.VISIBLE
                        rating  = (pos * 5 / (pos + neg))
                    }
                }
            }

        }
    }

    fun setListener(newListener : (RecipeDetail) -> Unit) {
        listener = newListener
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<RecipeDetail>() {
            override fun areItemsTheSame(oldItem: RecipeDetail, newItem: RecipeDetail): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: RecipeDetail, newItem: RecipeDetail): Boolean = oldItem == newItem
        }
    }
}

class RecipeSearchViewHolder(val binding : ListItemRecipeSearchBinding) : RecyclerView.ViewHolder(binding.root)