package arrowcarrier.recepty.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import arrowcarrier.recepty.dataClasses.DatabaseRecipe
import arrowcarrier.recepty.databinding.ListItemRecipeGroupBinding

class MyRecipesAdapter(private val listener : (Long) -> Unit) : PagingDataAdapter<DatabaseRecipe, RecipeGroupViewHolder>(

    DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeGroupViewHolder {
        return RecipeGroupViewHolder(ListItemRecipeGroupBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecipeGroupViewHolder, position: Int) {
        val recipe = getItem(position)
        recipe?.let {
            holder.binding.apply {
                ivThumbnailGroup.setImageBitmap(it.thumbnail)
                tvNameGroup.text = it.recipeName
                root.setOnClickListener {
                    listener(recipe.recipeId)
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<DatabaseRecipe>() {
            override fun areItemsTheSame(oldItem: DatabaseRecipe, newItem: DatabaseRecipe): Boolean = oldItem.recipeId == newItem.recipeId

            override fun areContentsTheSame(oldItem: DatabaseRecipe, newItem: DatabaseRecipe): Boolean = oldItem == newItem
        }
    }
}