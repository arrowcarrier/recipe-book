package arrowcarrier.recepty.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import arrowcarrier.recepty.databinding.ListItemInstructionBinding

class InstructionAdapter(private val list : List<String>) : RecyclerView.Adapter<InstructionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstructionViewHolder {
        return InstructionViewHolder(ListItemInstructionBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: InstructionViewHolder, position: Int) {
        holder.binding.tvInstruction.text = list[position]
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class InstructionViewHolder(val binding : ListItemInstructionBinding) : RecyclerView.ViewHolder(binding.root)