package arrowcarrier.recepty.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import arrowcarrier.recepty.R
import arrowcarrier.recepty.databinding.LoadingStateRecipeBinding
import java.io.IOException

class LoadingStateAdapter(private val retry : () -> Unit) : LoadStateAdapter<LoadingStateViewHolder>() {
    override fun onBindViewHolder(holder: LoadingStateViewHolder, loadState: LoadState) {
        holder.binding.apply {
            val message = if (loadState is LoadState.Error) {
                if(loadState.error is IOException)
                    holder.binding.root.context.resources.getString(R.string.no_connection_title) else
                    holder.binding.root.context.resources.getString(R.string.unknown_error_title)
            } else ""
            tvErrorMessage.text = message
            tvErrorMessage.isVisible = loadState is LoadState.Error
            btnRetry.isVisible = loadState is LoadState.Error
            pbLoading.isVisible = loadState is LoadState.Loading
            btnRetry.setOnClickListener { retry() }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): LoadingStateViewHolder {
        return LoadingStateViewHolder(LoadingStateRecipeBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }
}

class LoadingStateViewHolder(val binding : LoadingStateRecipeBinding) : RecyclerView.ViewHolder(binding.root)