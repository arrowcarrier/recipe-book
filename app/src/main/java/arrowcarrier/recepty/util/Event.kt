package arrowcarrier.recepty.util

class Event<out T>(private val data : T) {

    var wasHandled = false
        private set

    fun getContentIfNotHandled() : T? {
        return if (wasHandled) {
            null
        } else {
            wasHandled = true
            data
        }
    }
}