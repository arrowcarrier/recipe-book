package arrowcarrier.recepty.util

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.widget.ImageView
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import arrowcarrier.recepty.dataClasses.*
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

fun ArrayList<Tag>.getCheckedTagsAsString(displayName : Boolean) : String {
    var string = ""
    forEach{ tag ->
        string += if (displayName) "${tag.displayName}, " else "${tag.name}, "
    }
    string = string.dropLast(2)
    return string
}

fun RecipeDetail.getTagsAsString() : String {
    var string = ""
    tags.forEach{ tag ->
        string += "${tag.displayName}, "
    }
    string = string.dropLast(2)
    return string
}

fun RecipeDetail.getIngredientsAsString() : String {
    var string = ""
    ingredients.forEach { section ->
        section.components.forEach {
            string += "${it.text}\n"
        }
    }
    return string
}

fun Nutrition.isValid() : Boolean {
    return (calories != 0 && carbohydrates != 0 && fat != 0 && protein != 0 && sugar != 0 && protein != 0)
}

fun ImageView.downloadAndSaveImage(glide : RequestManager, recipe : RecipeDetail, saveButton : ImageView) {
    saveButton.isEnabled = false
    if (recipe.recipeImage == null) {
        glide.load(recipe.thumbnail)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    saveButton.isEnabled = true
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    recipe.recipeImage = resource?.toBitmap()
                    saveButton.isEnabled = true
                    return false
                }
            })
            .into(this)
    } else {
        this.setImageDrawable(BitmapDrawable(null, recipe.recipeImage))
        saveButton.isEnabled = true
    }
}


fun RecipeDetail.asDatabaseRecipe() : DatabaseRecipe {
    return DatabaseRecipe(0, name, description, recipeImage, video, portions)
}

fun List<Section>.asDatabaseIngredientList(recipeId : Long) : List<DatabaseIngredient> {
    val list = ArrayList<DatabaseIngredient>()
    forEach {  section ->
        section.components.forEach { component ->
            list.add(DatabaseIngredient(0L, recipeId, component.text))
        }
    }
    return list
}

fun Nutrition.asDatabaseNutrition(recipeId : Long) : DatabaseNutrition {
    return DatabaseNutrition(recipeId, calories, carbohydrates, fat, protein, sugar, fiber)
}

fun List<DatabaseTag>.asString() : String {
    var string = ""
    forEach{ tag ->
        string += "${tag.displayName}, "
    }
    string = string.dropLast(2)
    return string
}

fun DatabaseNutrition.isValid() : Boolean {
    return (calories != 0 && carbohydrates != 0 && fat != 0 && protein != 0 && sugar != 0 && protein != 0)
}

fun List<DatabaseIngredient>.getIngredientsAsString() : String {
    var string = ""
        forEach {
            string += "${it.text}\n"
        }
    return string
}

fun Fragment.isDeviceConnectedToInternet() : Boolean {
    val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = connectivityManager.activeNetwork ?: return false
    val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
    return when {
        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
        else -> false
    }
}
