package arrowcarrier.recepty.util


sealed class LivedataResponse<out T> {
    object Loading : LivedataResponse<Nothing>()
    data class Error<out T: Any>(val data : T?, val exception: Throwable?) : LivedataResponse<T>()
    data class Success<out T: Any>(val data : T?) : LivedataResponse<T>()
}