package arrowcarrier.recepty.di

import android.content.Context
import androidx.room.Room
import arrowcarrier.recepty.R
import arrowcarrier.recepty.db.RecipeDatabase
import arrowcarrier.recepty.network.RetrofitService
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofitInstance() = RetrofitService.create()

    @Singleton
    @Provides
    fun provideGlideInstance(
        @ApplicationContext context : Context
    ) = Glide.with(context).setDefaultRequestOptions(
        RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.loading_image)
            .error(R.drawable.error_image)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
    )

    @Singleton
    @Provides
    fun provideDataSourceFactory(
        @ApplicationContext context : Context
    ) = DefaultDataSourceFactory(context, Util.getUserAgent(context, context.getString(R.string.app_name)))

    @Provides
    fun provideExoPlayer(
        @ApplicationContext context: Context
    ) = SimpleExoPlayer.Builder(context).build()

    @Provides
    fun provideRecipeDAO(
        recipeDatabase : RecipeDatabase
    ) = recipeDatabase.recipeDao()

    @Singleton
    @Provides
    fun provideRecipeDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        RecipeDatabase::class.java,
        "recipe_database"
    ).build()


}