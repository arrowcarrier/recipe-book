package arrowcarrier.recepty.di

import android.content.Context
import arrowcarrier.recepty.R
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object SearchModule {

    @Provides
    @ViewModelScoped
    fun provideAvailableTagGroups(
        @ApplicationContext context : Context
    ): Array<String> = context.resources.getStringArray(R.array.available_tag_groups)
}