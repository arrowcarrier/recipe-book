package arrowcarrier.recepty.views

import android.content.Context
import android.util.AttributeSet
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.PopupMenu
import arrowcarrier.recepty.R

class ToggleGroupMenu(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    com.google.android.material.button.MaterialButtonToggleGroup(context, attrs, defStyleAttr) {

    constructor(context : Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context : Context) : this(context, null, 0)

    private lateinit var menu : Menu

    fun initMenu(resId : Int) {
        val pMenu = PopupMenu(context, null)
        menu = pMenu.menu
        MenuInflater(context).inflate(resId, menu)
    }

    fun getMenuItem(id : Int) : MenuItem {
        return when(id) {
            R.id.btn_description -> menu.getItem(0)
            R.id.btn_info -> menu.getItem(1)
            R.id.btn_ingredients -> menu.getItem(2)
            R.id.btn_instructions -> menu.getItem(3)
            else -> menu.getItem(4)
        }
    }
}
