package arrowcarrier.recepty.ui.detailContentFragments.dbDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import arrowcarrier.recepty.R
import arrowcarrier.recepty.databinding.FragmentVideoBinding
import arrowcarrier.recepty.ui.dialogs.VideoFullscreenDialog
import arrowcarrier.recepty.util.isDeviceConnectedToInternet
import arrowcarrier.recepty.viewmodels.RecipeDbViewModel

class DbVideoFragment : Fragment() {

    private val viewModel : RecipeDbViewModel by viewModels( ownerProducer = { requireParentFragment().requireParentFragment() })

    private lateinit var binding : FragmentVideoBinding

    private val fullscreenDialog : VideoFullscreenDialog by lazy { VideoFullscreenDialog(requireContext(), ::dialogBackPressed) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentVideoBinding.inflate(inflater, container, false)

        binding.pvVideo.findViewById<ImageButton>(R.id.exo_fullscreen_icon).setOnClickListener {
            viewModel.changeVideoFullscreen()
        }

        viewModel.isVideoFullscreen.observe(viewLifecycleOwner, { isFullscreen ->
            if (isFullscreen) {
                binding.pvVideo.player = null
                fullscreenDialog.showDialog(viewModel.player)
            } else {
                fullscreenDialog.dismissDialog()
                binding.pvVideo.player = viewModel.player
            }
        })

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        if (viewModel.isVideoFullscreen.value == true) fullscreenDialog.showDialog(viewModel.player)
        viewModel.resumeVideo()
        if (!isDeviceConnectedToInternet()) Toast.makeText(requireContext(),
            resources.getString(R.string.toast_video_no_internet),
            Toast.LENGTH_LONG)
            .show()
    }

    override fun onStop() {
        super.onStop()
        fullscreenDialog.dismissDialog()
        viewModel.stopVideo()
    }

    private fun dialogBackPressed() {
        viewModel.changeVideoFullscreen()
    }
}