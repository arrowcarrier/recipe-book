package arrowcarrier.recepty.ui.detailContentFragments.networkDetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import arrowcarrier.recepty.R
import arrowcarrier.recepty.databinding.FragmentInfoBinding
import arrowcarrier.recepty.util.getTagsAsString
import arrowcarrier.recepty.util.isValid
import arrowcarrier.recepty.viewmodels.RecipeNetworkViewModel

class NetworkInfoFragment : Fragment() {

    private val viewModel : RecipeNetworkViewModel by viewModels( ownerProducer = { requireParentFragment().requireParentFragment() })

    private lateinit var binding : FragmentInfoBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentInfoBinding.inflate(inflater, container, false)

        viewModel.actualRecipe.observe(viewLifecycleOwner, { recipe ->
            binding.apply {
                tvTagContent.text = recipe.getTagsAsString()
                tvPortionsContent.text = recipe.portions.toString()
                if (recipe.nutrition != null && recipe.nutrition.isValid()) {
                    tvNutritionsContent.text =
                        resources.getString(R.string.nutritions_content,
                            recipe.nutrition.calories,
                            recipe.nutrition.carbohydrates,
                            recipe.nutrition.fat,
                            recipe.nutrition.protein,
                            recipe.nutrition.sugar,
                            recipe.nutrition.fiber,
                    )
                } else {
                    tvNutritionsTitle.visibility = View.GONE
                    tvNutritionsContent.visibility = View.GONE
                }
            }
        })

        return binding.root
    }

}