package arrowcarrier.recepty.ui.detailContentFragments.dbDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import arrowcarrier.recepty.R
import arrowcarrier.recepty.databinding.FragmentInfoBinding
import arrowcarrier.recepty.util.asString
import arrowcarrier.recepty.util.isValid
import arrowcarrier.recepty.viewmodels.RecipeDbViewModel

class DbInfoFragment : Fragment() {

    private val viewModel : RecipeDbViewModel by viewModels( ownerProducer = { requireParentFragment().requireParentFragment() })

    private lateinit var binding : FragmentInfoBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentInfoBinding.inflate(inflater, container, false)

        viewModel.actualRecipe.observe(viewLifecycleOwner, { relation ->
            binding.apply {
                tvTagContent.text = relation.tags.asString()
                tvPortionsContent.text = relation.recipe.portions.toString()
                if (relation.nutritions != null && relation.nutritions.isValid()) {
                    tvNutritionsContent.text =
                        resources.getString(
                            R.string.nutritions_content,
                            relation.nutritions.calories,
                            relation.nutritions.carbohydrates,
                            relation.nutritions.fat,
                            relation.nutritions.protein,
                            relation.nutritions.sugar,
                            relation.nutritions.fiber,
                        )
                } else {
                    tvNutritionsTitle.visibility = View.GONE
                    tvNutritionsContent.visibility = View.GONE
                }
            }
        })

        return binding.root
    }
}