package arrowcarrier.recepty.ui.detailContentFragments.networkDetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import arrowcarrier.recepty.databinding.FragmentDescriptionBinding
import arrowcarrier.recepty.viewmodels.RecipeNetworkViewModel

class NetworkDescriptionFragment : Fragment() {

    private val viewModel : RecipeNetworkViewModel by viewModels( ownerProducer = { requireParentFragment().requireParentFragment() })

    private lateinit var binding : FragmentDescriptionBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDescriptionBinding.inflate(inflater, container, false)

        viewModel.actualRecipe.observe(viewLifecycleOwner, { recipe ->
            binding.tvDescription.text = recipe.description
        })

        return binding.root
    }

}