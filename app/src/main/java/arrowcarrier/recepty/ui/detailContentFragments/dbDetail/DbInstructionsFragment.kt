package arrowcarrier.recepty.ui.detailContentFragments.dbDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import arrowcarrier.recepty.adapters.InstructionAdapter
import arrowcarrier.recepty.databinding.FragmentInstructionsBinding
import arrowcarrier.recepty.viewmodels.RecipeDbViewModel

class DbInstructionsFragment : Fragment() {

    private val viewModel : RecipeDbViewModel by viewModels( ownerProducer = { requireParentFragment().requireParentFragment() })

    private lateinit var binding : FragmentInstructionsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentInstructionsBinding.inflate(inflater, container, false)

        viewModel.actualRecipe.observe(viewLifecycleOwner, { relation ->
            binding.vpInstructions.adapter =
                InstructionAdapter(relation.instructions.map { it.text })
        })

        return binding.root
    }
}