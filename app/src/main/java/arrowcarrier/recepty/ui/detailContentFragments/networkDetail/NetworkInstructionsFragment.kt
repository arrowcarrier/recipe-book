package arrowcarrier.recepty.ui.detailContentFragments.networkDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import arrowcarrier.recepty.adapters.InstructionAdapter
import arrowcarrier.recepty.databinding.FragmentInstructionsBinding
import arrowcarrier.recepty.viewmodels.RecipeNetworkViewModel

class NetworkInstructionsFragment : Fragment() {

    private val viewModel : RecipeNetworkViewModel by viewModels( ownerProducer = { requireParentFragment().requireParentFragment() })

    private lateinit var binding : FragmentInstructionsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentInstructionsBinding.inflate(inflater, container, false)

        viewModel.actualRecipe.observe(viewLifecycleOwner, { recipe ->
            binding.vpInstructions.adapter =
                InstructionAdapter(recipe.instructions.map { it.text })
        })

        return binding.root
    }

}