package arrowcarrier.recepty.ui.detailContentFragments.dbDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import arrowcarrier.recepty.databinding.FragmentIngredientsBinding
import arrowcarrier.recepty.util.getIngredientsAsString
import arrowcarrier.recepty.viewmodels.RecipeDbViewModel

class DbIngredientsFragment : Fragment() {

    private val viewModel : RecipeDbViewModel by viewModels( ownerProducer = { requireParentFragment().requireParentFragment() })

    private lateinit var binding : FragmentIngredientsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentIngredientsBinding.inflate(inflater, container, false)

        viewModel.actualRecipe.observe(viewLifecycleOwner, { relation ->
            binding.tvIngredients.text = relation.ingredients.getIngredientsAsString()
        })

        return binding.root
    }
}