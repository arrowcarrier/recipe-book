package arrowcarrier.recepty.ui.detailContentFragments.dbDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import arrowcarrier.recepty.databinding.FragmentDescriptionBinding
import arrowcarrier.recepty.viewmodels.RecipeDbViewModel

class DbDescriptionFragment : Fragment() {

    private val viewModel : RecipeDbViewModel by viewModels( ownerProducer = { requireParentFragment().requireParentFragment() })

    private lateinit var binding : FragmentDescriptionBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDescriptionBinding.inflate(inflater, container, false)

        viewModel.actualRecipe.observe(viewLifecycleOwner, { relation ->
            binding.tvDescription.text = relation.recipe.description
        })

        return binding.root
    }
}