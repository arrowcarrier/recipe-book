package arrowcarrier.recepty.ui.detailContentFragments.networkDetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import arrowcarrier.recepty.databinding.FragmentIngredientsBinding
import arrowcarrier.recepty.util.getIngredientsAsString
import arrowcarrier.recepty.viewmodels.RecipeNetworkViewModel

class NetworkIngredientsFragment : Fragment() {

    private val viewModel : RecipeNetworkViewModel by viewModels( ownerProducer = { requireParentFragment().requireParentFragment() })

    private lateinit var binding : FragmentIngredientsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentIngredientsBinding.inflate(inflater, container, false)

        viewModel.actualRecipe.observe(viewLifecycleOwner, { recipe ->
            binding.tvIngredients.text = recipe.getIngredientsAsString()
        })

        return binding.root
    }
}