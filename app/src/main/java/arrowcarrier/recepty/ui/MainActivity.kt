package arrowcarrier.recepty.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import arrowcarrier.recepty.R
import arrowcarrier.recepty.databinding.ActivityMainBinding
import arrowcarrier.recepty.viewmodels.MainMotionStateViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel : MainMotionStateViewModel by viewModels()

    private lateinit var binding : ActivityMainBinding

    private var doubleBackPressed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val fragment = supportFragmentManager.findFragmentById(R.id.nhf_main) as NavHostFragment
        binding.bottomNavigationView.setupWithNavController(fragment.navController)

        viewModel.isAnimationStart.observe(this, {
            if (it) binding.mlMain.transitionToStart() else binding.mlMain.transitionToEnd()
        })
    }

    override fun onBackPressed() {
        if(!onBackPressedDispatcher.hasEnabledCallbacks()) {
            if (doubleBackPressed) {
                super.onBackPressed()
            } else {
                doubleBackPressed = true
                Toast.makeText(this, resources.getString(R.string.quit_app), Toast.LENGTH_SHORT).show()
                lifecycleScope.launch(Dispatchers.Default) {
                    delay(2000)
                    doubleBackPressed = false
                }
            }
        } else super.onBackPressed()
    }
}