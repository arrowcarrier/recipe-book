package arrowcarrier.recepty.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import arrowcarrier.recepty.R
import arrowcarrier.recepty.adapters.RecipeGroupAdapter
import arrowcarrier.recepty.dataClasses.RecipeDetail
import arrowcarrier.recepty.databinding.FragmentRecipeGroupBinding
import arrowcarrier.recepty.viewmodels.MainMotionStateViewModel
import arrowcarrier.recepty.viewmodels.RecipeGroupViewModel
import arrowcarrier.recepty.viewmodels.ViewModelService
import com.bumptech.glide.RequestManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RecipeGroupFragment : Fragment() {

    private lateinit var mainViewModel : MainMotionStateViewModel

    private val args : RecipeGroupFragmentArgs by navArgs()

    private val viewModel : RecipeGroupViewModel by viewModels{
        ViewModelService.getRecipeGroupViewModel(args.recipe)
    }

    @Inject
    lateinit var glide : RequestManager

    @Inject
    lateinit var adapter : RecipeGroupAdapter

    private lateinit var binding : FragmentRecipeGroupBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecipeGroupBinding.inflate(inflater, container, false)

        mainViewModel = ViewModelProvider(requireActivity()).get(MainMotionStateViewModel::class.java)

        adapter.setListener(::recipeClicked)

        viewModel.actualRecipe.observe(viewLifecycleOwner, { group ->

            binding.apply {
                ivBackGroup.setOnClickListener {
                    (activity as MainActivity).onBackPressed()
                }

                glide.load(group.thumbnail)
                    .into(ivGroupImage)
                tvGroupName.text = group.name
                val decisionText = requireContext().resources.getString(R.string.recipe_group_decision, group.recipes?.size)
                tvChooseRecipe.text = decisionText

                rvGroupRecipes.adapter = adapter
                adapter.setFeed(group.recipes!!)
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                mainViewModel.changeAnimationState()
                parentFragment?.findNavController()?.popBackStack()
            }
        })

        return binding.root
    }

    private fun recipeClicked(recipe : RecipeDetail) {
        val action = RecipeGroupFragmentDirections.actionRecipeGroupFragmentToRecipeNetworkDetailFragment(recipe, false)
        parentFragment?.findNavController()?.navigate(action)
    }
}