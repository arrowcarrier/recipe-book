package arrowcarrier.recepty.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import arrowcarrier.recepty.R
import arrowcarrier.recepty.databinding.FragmentRecipeDetailBinding
import arrowcarrier.recepty.util.downloadAndSaveImage
import arrowcarrier.recepty.viewmodels.RecipeNetworkViewModel
import com.bumptech.glide.RequestManager
import com.google.android.material.button.MaterialButtonToggleGroup
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RecipeNetworkDetailFragment : RecipeDetailFragment() {

    private val args: RecipeNetworkDetailFragmentArgs by navArgs()

    private val viewModel: RecipeNetworkViewModel by viewModels()

    @Inject
    lateinit var glide: RequestManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecipeDetailBinding.inflate(layoutInflater, container, false)

        viewModel.setRecipeDetail(args.recipe)

        toggleListener = MaterialButtonToggleGroup.OnButtonCheckedListener { _, id, isChecked ->
            if (isChecked) {
                toggleListenerAction(id)
                viewModel.changeMenuState()
            }
        }

        initialize()

        binding.apply {
            val navfragment =
                childFragmentManager.findFragmentById(R.id.nhf_detail) as NavHostFragment
            navfragment.navController.setGraph(R.navigation.network_detail_navigation)
            tgMenu.initMenu(R.menu.network_recipe_detail_menu)
            ivMenu.setOnClickListener {
                if (viewModel.isMenuRolledUp) mlDetail.transitionToStart() else mlDetail.transitionToEnd()
                viewModel.changeMenuState()
            }
            ivManage.setOnClickListener {
                viewModel.saveRecipeDetail(mainViewModel.enabledTags)
            }
        }

        viewModel.actualRecipe.observe(viewLifecycleOwner, { recipe ->
            binding.apply {
                ivDetailImage.downloadAndSaveImage(glide, recipe, ivManage)

                tvDetailName.text = recipe.name

                lifecycleScope.launch {
                    val job = async { !viewModel.containsRecipe(recipe.name) }
                    ivManage.isVisible = job.await()
                }

                btnDescription.isVisible = (recipe.description != null && recipe.description != "")
                btnVideo.isVisible = (recipe.video != null && recipe.video != "")

                displayToggleFragment(viewModel.lastOpenedMenuFragment)
                viewModel.changeMenuState()
            }

        })

        viewModel.recipeOperationState.observe(viewLifecycleOwner, { response ->
            handleDbOperationResponse(
                response,
                R.string.toast_save_error,
                R.string.toast_save_success
            )
        })

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (args.fromSearch) mainViewModel.changeAnimationState()
                    parentFragment?.findNavController()?.popBackStack()
                }
            })

        return binding.root
    }

    override fun onStop() {
        super.onStop()
        viewModel.lastOpenedMenuFragment = binding.tgMenu.checkedButtonId
    }
}
