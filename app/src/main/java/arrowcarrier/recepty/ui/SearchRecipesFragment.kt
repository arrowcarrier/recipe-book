package arrowcarrier.recepty.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import arrowcarrier.recepty.R
import arrowcarrier.recepty.adapters.LoadingStateAdapter
import arrowcarrier.recepty.adapters.RecipeSearchAdapter
import arrowcarrier.recepty.adapters.TagGroupAdapter
import arrowcarrier.recepty.dataClasses.*
import arrowcarrier.recepty.databinding.FragmentSearchRecipesBinding
import arrowcarrier.recepty.util.LivedataResponse
import arrowcarrier.recepty.util.getCheckedTagsAsString
import arrowcarrier.recepty.viewmodels.MainMotionStateViewModel
import arrowcarrier.recepty.viewmodels.SearchRecipeViewModel
import com.google.android.material.button.MaterialButtonToggleGroup
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class SearchRecipesFragment : ErrorDialogFragment() {

    private lateinit var mainViewModel : MainMotionStateViewModel

    private val searchRecipesViewModel: SearchRecipeViewModel by viewModels()

    private lateinit var binding: FragmentSearchRecipesBinding

    private val tagAdapter: TagGroupAdapter by lazy {
        TagGroupAdapter(
            emptyList(),
            ::changeTagCheck
        )
    }

    @Inject
    lateinit var recipeAdapter : RecipeSearchAdapter

    private lateinit var toggleListener: MaterialButtonToggleGroup.OnButtonCheckedListener

    private var pagingJob : Job? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchRecipesBinding.inflate(inflater, container, false)

        mainViewModel = ViewModelProvider(requireActivity()).get(MainMotionStateViewModel::class.java)

        binding.apply {
            btnSearch.setOnClickListener {
                searchRecipes()
                searchRecipesViewModel.changeMotionProgress()
            }
            fabSearch.setOnClickListener {
                mlSearch.transitionToStart()
                searchRecipesViewModel.changeMotionProgress()
            }

            toggleListener = MaterialButtonToggleGroup.OnButtonCheckedListener { _, id, isChecked ->
                if (isChecked) {
                    tagAdapter.changeData(searchRecipesViewModel.getTagGroup(getTagGroupIndex(id)))
                }
            }
            tgSearch.addOnButtonCheckedListener(toggleListener)

            rvSearch.adapter = tagAdapter
            
            recipeAdapter.setListener(::recipeClicked)
            rvRecipes.adapter = recipeAdapter.withLoadStateFooter(LoadingStateAdapter{ recipeAdapter.retry() })

            mlSearch.progress = searchRecipesViewModel.motionProgress

            tvSearchSearchedTags.text =
                resources.getString(R.string.fragment_search_tag_combination, searchRecipesViewModel.checkedTags.getCheckedTagsAsString(true))
        }

        searchRecipesViewModel.tagResponse.observe(viewLifecycleOwner, { response ->
            when(response) {
                is LivedataResponse.Loading -> showProgressBar(binding.pbSearch)
                is LivedataResponse.Error -> {
                    hideProgressBar(binding.pbSearch)
                    showErrorDialog(response.exception, searchRecipesViewModel::loadTags, ::dialogNegativeAnswer)
                }
                is LivedataResponse.Success -> {
                    mainViewModel.enabledTags = searchRecipesViewModel.getEnabledTags()
                    hideProgressBar(binding.pbSearch)
                    binding.tgSearch.check(R.id.btn_appliance)
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launch {
            recipeAdapter.loadStateFlow.collectLatest { newLoadState ->
                when(newLoadState.refresh) {
                    is LoadState.Loading -> showProgressBar(binding.pbRecipeLoading)
                    is LoadState.NotLoading -> {
                        hideProgressBar(binding.pbRecipeLoading)
                        if (newLoadState.append.endOfPaginationReached &&
                            recipeAdapter.itemCount < 1) {
                                binding.tvNoResult.visibility = View.VISIBLE
                        }
                    }
                    is LoadState.Error -> {
                        hideProgressBar(binding.pbRecipeLoading)
                        showErrorDialog((newLoadState.refresh as LoadState.Error).error, recipeAdapter::retry, ::dialogNegativeAnswer)
                    }

                }
            }
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        if (searchRecipesViewModel.isReloadNeeded()) collectPagingFlow()
    }

    override fun onDestroyView() {
        binding.tgSearch.removeOnButtonCheckedListener(toggleListener)
        super.onDestroyView()
    }

    private fun changeTagCheck(tag : Tag, isChecked : Boolean) {
        if(isChecked) searchRecipesViewModel.checkedTags.add(tag) else searchRecipesViewModel.checkedTags.remove(tag)
    }

    private fun showProgressBar(progressBar : ProgressBar) {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar(progressBar : ProgressBar) {
        progressBar.visibility = View.GONE
    }

    private fun getTagGroupIndex(id : Int) : Int {
        return when(id) {
            R.id.btn_appliance -> 0
            R.id.btn_cuisine -> 1
            R.id.btn_dietary -> 2
            R.id.btn_difficulty -> 3
            R.id.btn_equipment -> 4
            R.id.btn_holiday -> 5
            R.id.btn_meal -> 6
            R.id.btn_method -> 7
            R.id.btn_occasion -> 8
            R.id.btn_seasonal -> 9
            else -> -1

        }
    }

    private fun searchRecipes() {
        binding.apply {
            mlSearch.transitionToEnd()
            recipeAdapter.submitData(lifecycle, PagingData.empty())
            tvSearchSearchedTags.text =
                resources.getString(R.string.fragment_search_tag_combination, searchRecipesViewModel.checkedTags.getCheckedTagsAsString(true))
            tvNoResult.visibility = View.GONE
        }
        pagingJob?.cancel()
        collectPagingFlow()
    }

    private fun recipeClicked(recipe : RecipeDetail) {
        mainViewModel.changeAnimationState()
        val action = if(recipe.recipes != null) SearchRecipesFragmentDirections.actionSearchRecipesFragmentToRecipeGroupFragment(recipe)
            else SearchRecipesFragmentDirections.actionSearchRecipesFragmentToRecipeNetworkDetailFragment(recipe, true)
        parentFragment?.findNavController()?.navigate(action)
    }

    private fun collectPagingFlow() {
        pagingJob = viewLifecycleOwner.lifecycleScope.launch {
            searchRecipesViewModel.searchAPIPaging().collectLatest {
                hideProgressBar(binding.pbRecipeLoading)
                recipeAdapter.submitData(it)
            }
        }
    }

    private fun dialogNegativeAnswer() {
        (activity as MainActivity).onBackPressed()
    }

}