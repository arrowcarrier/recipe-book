package arrowcarrier.recepty.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import arrowcarrier.recepty.adapters.LoadingStateAdapter
import arrowcarrier.recepty.adapters.MyRecipesAdapter
import arrowcarrier.recepty.databinding.FragmentMyRecipesListBinding
import arrowcarrier.recepty.viewmodels.MainMotionStateViewModel
import arrowcarrier.recepty.viewmodels.MyRecipesViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MyRecipesListFragment : ErrorDialogFragment() {

    private lateinit var mainViewModel : MainMotionStateViewModel

    private lateinit var binding : FragmentMyRecipesListBinding

    private val viewModel : MyRecipesViewModel by viewModels()

    private val recipeAdapter = MyRecipesAdapter(::recipeClicked)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMyRecipesListBinding.inflate(inflater, container, false)

        mainViewModel = ViewModelProvider(requireActivity()).get(MainMotionStateViewModel::class.java)

        binding.apply {
            rvSavedRecipes.adapter = recipeAdapter.withLoadStateFooter(LoadingStateAdapter {recipeAdapter.retry()} )
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.searchDbPaging().collectLatest {
                recipeAdapter.submitData(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            recipeAdapter.loadStateFlow.collectLatest { state ->
                binding.apply {
                    when(state.refresh) {
                        is LoadState.Loading -> pbMyRecipes.visibility = View.VISIBLE
                        is LoadState.NotLoading -> {
                            pbMyRecipes.visibility = View.GONE
                            if (state.append.endOfPaginationReached &&
                                recipeAdapter.itemCount < 1) {
                                tvNoSavedRecipes.visibility = View.VISIBLE
                            }
                        }
                        is LoadState.Error -> {
                            pbMyRecipes.visibility = View.GONE
                            showErrorDialog((state.refresh as LoadState.Error).error, recipeAdapter::retry)
                        }
                    }
                }

            }
        }

        return binding.root
    }

    private fun recipeClicked(recipeId : Long) {
        mainViewModel.changeAnimationState()
        val action = MyRecipesListFragmentDirections.actionMyRecipesListFragmentToRecipeDbDetailFragment(recipeId)
        parentFragment?.findNavController()?.navigate(action)
    }

}