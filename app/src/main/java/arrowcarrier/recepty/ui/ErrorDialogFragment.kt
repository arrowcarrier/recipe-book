package arrowcarrier.recepty.ui

import android.app.AlertDialog
import androidx.fragment.app.Fragment
import arrowcarrier.recepty.R
import java.io.IOException

abstract class ErrorDialogFragment : Fragment() {

    protected fun showErrorDialog(exception : Throwable?, positiveAnswer : () -> Unit, negativeAnswer : (() -> Unit) = {}) {
        val title = if (exception is IOException) R.string.no_connection_title else R.string.unknown_error_title
        val message = if(exception is IOException) resources.getString(R.string.no_connection_message)  else exception?.message
        val icon = if(exception is IOException) R.drawable.ic_connection_error else R.drawable.ic_error
        showDialog(title, message, icon, positiveAnswer, negativeAnswer)
    }

    private fun showDialog(title : Int, message : String?, icon : Int, positiveAnswer : () -> Unit, negativeAnswer : (() -> Unit)? = null) {
        AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setIcon(icon)
            .setPositiveButton(R.string.positive_answer) { _, _ ->
                positiveAnswer()
            }
            .setNegativeButton(R.string.negative_answer) { _, _ ->
                negativeAnswer?.invoke()
            }
            .setCancelable(false)
            .show()
    }
}