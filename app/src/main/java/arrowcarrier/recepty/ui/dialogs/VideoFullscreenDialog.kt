package arrowcarrier.recepty.ui.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageButton
import arrowcarrier.recepty.R
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlayerView

class VideoFullscreenDialog(context : Context, private val backPress: () -> Unit) : Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_fullscreen_video)
    }

    override fun show() {
        window?.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        super.show()
        window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
    }

    override fun onBackPressed() {
        backPress()
    }

    fun showDialog(newPlayer : Player) {
        if (!isShowing) {
            show()
            val playerView = findViewById<PlayerView>(R.id.fullscreen_player).also {
                it.player = newPlayer
            }
            playerView.findViewById<ImageButton>(R.id.exo_fullscreen_icon).apply {
                setImageResource(R.drawable.ic_fullscreen_close)
                setOnClickListener {
                    backPress()
                }
            }

        }
    }

    fun dismissDialog() {
        if (isShowing) {
            findViewById<PlayerView>(R.id.fullscreen_player).player = null
            dismiss()
        }
    }

}