package arrowcarrier.recepty.ui

import android.view.View
import android.widget.Toast
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import arrowcarrier.recepty.R
import arrowcarrier.recepty.databinding.FragmentRecipeDetailBinding
import arrowcarrier.recepty.util.Event
import arrowcarrier.recepty.util.LivedataResponse
import arrowcarrier.recepty.viewmodels.MainMotionStateViewModel
import com.google.android.material.button.MaterialButtonToggleGroup

abstract class RecipeDetailFragment : Fragment(), MotionLayout.TransitionListener {

    protected lateinit var mainViewModel : MainMotionStateViewModel

    protected lateinit var binding : FragmentRecipeDetailBinding

    protected lateinit var toggleListener: MaterialButtonToggleGroup.OnButtonCheckedListener

    protected fun initialize() {
        mainViewModel = ViewModelProvider(requireActivity()).get(MainMotionStateViewModel::class.java)

        binding.mlDetail.addTransitionListener(this)

        binding.apply {
            ivBackDetail.setOnClickListener {
                activity?.onBackPressed()
            }
            tgMenu.addOnButtonCheckedListener(toggleListener)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.tgMenu.removeOnButtonCheckedListener(toggleListener)
    }

    override fun onTransitionStarted(motionLayout: MotionLayout?, startId: Int, endId: Int) {
        binding.ivMenu.isEnabled = false
    }

    override fun onTransitionChange(
        motionLayout: MotionLayout?,
        startId: Int,
        endId: Int,
        progress: Float
    ) { }

    override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
        binding.ivMenu.isEnabled = true
    }

    override fun onTransitionTrigger(
        motionLayout: MotionLayout?,
        triggerId: Int,
        positive: Boolean,
        progress: Float
    ) { }

    protected fun toggleListenerAction(id : Int) {
        val item = binding.tgMenu.getMenuItem(id)
        val fragment = childFragmentManager.findFragmentById(R.id.nhf_detail) as NavHostFragment
        NavigationUI.onNavDestinationSelected(item, fragment.navController)
        binding.mlDetail.transitionToStart()
    }

    protected fun displayToggleFragment(lastId : Int?) {
        binding.apply {
            when {
                lastId != null -> {
                    tgMenu.check(lastId)
                }
                btnDescription.isVisible -> tgMenu.check(R.id.btn_description)
                else -> tgMenu.check(R.id.btn_info)
            }
        }
    }

    protected fun handleDbOperationResponse(response : LivedataResponse<Event<String>>, errorMessageId : Int, successMessageId : Int) {
        when(response) {
            is LivedataResponse.Loading -> {}
            is LivedataResponse.Error -> {
                response.data?.getContentIfNotHandled()?.let {
                    val message = resources.getString(errorMessageId)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
                }

            }
            is LivedataResponse.Success -> {
                response.data?.getContentIfNotHandled()?.let {
                    binding.ivManage.apply {
                        visibility = View.INVISIBLE
                        isEnabled = false
                    }
                    val message = resources.getString(successMessageId, it)
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}