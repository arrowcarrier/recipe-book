package arrowcarrier.recepty.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import arrowcarrier.recepty.R
import arrowcarrier.recepty.databinding.FragmentRecipeDetailBinding
import arrowcarrier.recepty.viewmodels.RecipeDbViewModel
import com.google.android.material.button.MaterialButtonToggleGroup
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RecipeDbDetailFragment : RecipeDetailFragment() {

    val args : RecipeDbDetailFragmentArgs by navArgs()

    private val viewModel : RecipeDbViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecipeDetailBinding.inflate(layoutInflater, container, false)

        viewModel.findRecipeDetail(args.recipeId)


        toggleListener = MaterialButtonToggleGroup.OnButtonCheckedListener { _, id, isChecked ->
            if (isChecked) {
                toggleListenerAction(id)
                viewModel.changeMenuState()
            }
        }

        initialize()

        binding.apply {
            val navfragment = childFragmentManager.findFragmentById(R.id.nhf_detail) as NavHostFragment
            navfragment.navController.setGraph(R.navigation.db_detail_navigation)
            tgMenu.initMenu(R.menu.db_recipe_detail_menu)
            ivMenu.setOnClickListener {
                if (viewModel.isMenuRolledUp) mlDetail.transitionToStart() else mlDetail.transitionToEnd()
                viewModel.changeMenuState()
            }
            ivManage.apply {
                setImageResource(R.drawable.ic_delete)
                setOnClickListener {
                        viewModel.deleteRecipeFromDatabase()
                }
            }

        }

        viewModel.actualRecipe.observe(viewLifecycleOwner, { relation ->
            binding.apply {
                ivDetailImage.setImageBitmap(relation.recipe.thumbnail)
                tvDetailName.text = relation.recipe.recipeName

                btnDescription.isVisible = (relation.recipe.description != null && relation.recipe.description != "")
                btnVideo.isVisible = (relation.recipe.video != null && relation.recipe.video != "")

                displayToggleFragment(viewModel.lastOpenedMenuFragment)
                viewModel.changeMenuState()
            }
        })

        viewModel.recipeOperationState.observe(viewLifecycleOwner, { state ->
            handleDbOperationResponse(state, R.string.toast_delete_error, R.string.toast_delete_success)
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                mainViewModel.changeAnimationState()
                parentFragment?.findNavController()?.popBackStack()
            }
        })

        return binding.root
    }

    override fun onStop() {
        super.onStop()
        viewModel.lastOpenedMenuFragment = binding.tgMenu.checkedButtonId
    }
}